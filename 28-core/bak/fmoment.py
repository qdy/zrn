import sys
import os
from scipy.integrate import simps
import numpy as np

def read_a2f(fname):
    x = []
    y = []

    with open(fname, 'r') as f:
        f.readline()
        for line in f:
            line = line.split()
            if len(line) != 6:
                x.append(float(line[0])) #*13.6*1e3)
                y.append(float(line[1]))
    return x, y

def find_moment(x, y, m):
    lam = simps([2e0 * y[i]/x[i] for i in range(len(x))], x)
    z = [y[i]/x[i] * x[i] ** m for i in range(len(x))]
    #print(m, (2e0/lam * simps(z, x))**(1e0/float(m)))

    return lam, (2e0/ lam * simps(z, x)) ** (1e0/float(m))

def find_log(x, y):
    lam = simps([2e0 * y[i] / x[i] for i in range(len(x))], x)
    z = [y[i] / x[i] * np.log(x[i]) for i in range(len(x))]
    #print ('log', np.exp(2e0/lam * simps(z, x)))

    wlog = np.exp( 2e0 / lam * simps(z, x))

    return wlog


if __name__ == '__main__':

    x, y = read_a2f(sys.argv[1])

    wm = [[], [], [], []]

    wm[0], wm[1]  = find_moment(x, y, 1)
    _, wm[2] = find_moment(x, y, 2)
    _, wm[3] = find_moment(x, y, 3)
    wlog = find_log( x, y)

    print ("Lambda      ", "{0:8.3f}".format(wm[0]))
    print ("w1     (meV)", "{0:8.3f}".format(wm[1]))
    print ("w2     (meV)", "{0:8.3f}".format(wm[2]))
    print ("w3     (meV)", "{0:8.3f}".format(wm[3]))
    print ("wlog   (meV)", "{0:8.3f}".format( wlog))
    print ("{0:3.2f}  & {1:4.3f} & {2:4.3f} & {3:4.3f} &".format( wm[0], wlog , wm[1],  wm[2]))
